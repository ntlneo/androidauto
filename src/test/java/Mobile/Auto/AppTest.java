package Mobile.Auto;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.concurrent.TimeUnit;

import org.aspectj.lang.annotation.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;



/**
 * Unit test for simple App.
 */
public class AppTest {
	AndroidDriver<WebElement> driver;
	MobileElement mElement;
	By inputField, resultField,num1,num2,num3,num4,add,subtract,multiply,divide,equalSign;
	
	
	
	@BeforeMethod
	public void runBeforeTest() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "Geny");
		capabilities.setCapability("automationName", "UiAutomator2");
//		capabilities.setCapability("platformName", "Android");
//		capabilities.setCapability("appPackage", "com.android.calculator2");
//		capabilities.setCapability("appActivity","com.android.calculator2.Calculator");
		capabilities.setCapability(AndroidMobileCapabilityType.PLATFORM_NAME, "Android");
		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.calculator2");
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,"com.android.calculator2.Calculator");	
		
		driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);

		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		setupLocator();
	}
	
//	@AfterMethod
//	public void runAfterTest() {
//		driver.quit();
//	}
	
	@Test
	public void testCalc() {
		click(num1);		
		click(add);
		click(num2);
		
		
	}
	
	public void setupLocator() {		
		inputField = By.id("com.android.calculator2:id/formula");
		resultField = By.id("com.android.calculator2:id/result");
		num1 = By.id("com.android.calculator2:id/digit_1");
		num2 = By.id("com.android.calculator2:id/digit_2");
		num3 = By.id("com.android.calculator2:id/digit_3");
		num4 = By.id("com.android.calculator2:id/digit_4");
		add = By.id("com.android.calculator2:id/op_add");
		subtract = By.id("com.android.calculator2:id/op_sub");
		multiply = By.id("com.android.calculator2:id/op_mul");
		divide = By.id("com.android.calculator2:id/op_div");
		equalSign = By.id("com.android.calculator2:id/eq");
	}
	
	public void click(By locator) {
		mElement = (MobileElement) driver.findElement(locator);
		mElement.click();
	}
	
	public void getmElement() {
		
	}
	
	public void verifyResult(int number ) {
//		if (number = )
	}
	
	

}
